def inp():
    a = input("Enter value in mm, cm, and m: ")
    t = input("Enter unit to convert in mm, cm, m: ")
    k = "".join([i for i in a if i in ["m","c"]])
    v = a[0:len(a)-len(k)]
    return [k,v,t]

def conv(o):
    if o[0] == "m":
        b = 1
    elif o[0] == "cm":
        b = 10**(-2)
    elif o[0] == "mm":
        b = 10**(-3)
    if o[2] == "m":
        c = 1
    elif o[2] == "cm":
        c = 10**(-2)
    elif o[2] == "mm":
        c = 10**(-3)
    return print(f"Value after unit conversion is {float(o[1])*(b/c)}{o[2]}")

conv((inp()))
